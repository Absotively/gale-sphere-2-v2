import { getPartyList } from "./lib/party.js";
import { drawCircle, drawRectangle, drawLine, drawPolyline } from "./lib/drawing.js";
import { SelfDrawingEntity } from "./lib/entities.js";

const lane_size = 5;
const orb_size = 4.5;
const orb_color = "rgb(44,197,146)";
const orb_outline_color ="rgb(142,255,217)";
const orb_hit_effect_color = "rgb(12,105,74)";
const clone_size = 7;
const max_clone_distance = 19;
const clone_travel_time = 2; // seconds
const clone_speed_yalms_per_second = max_clone_distance / clone_travel_time;

class OrbSet extends SelfDrawingEntity {
  constructor(origin, lanes) {
    super(0);
    this.origin = origin;
    this.lanes = lanes;
    this.clone_visible = true;
    this.orbs_visible = false;
    this.warnings_visible = false;
    this.hit_indicator_visible = false;
  
    this.clone_x = 100;
    this.clone_y = 100;
    this.clone_x_speed = 0;
    this.clone_y_speed = 0;
    
    switch(origin) {
      case "N":
        this.clone_y_speed = -1 * clone_speed_yalms_per_second;
        break;
      case "S":
        this.clone_y_speed = clone_speed_yalms_per_second;
        break;
      case "W":
        this.clone_x_speed = -1 * clone_speed_yalms_per_second;
        break;
      case "E":
        this.clone_x_speed = clone_speed_yalms_per_second;
    }
  }

  showOrbs() {
    this.orbs_visible = true;
    this.clone_visible = false;
  }

  showWarnings() {
    this.warnings_visible = true;
  }

  hit() {
    this.hit_indicator_visible = true;
    this.warnings_visible = false;
    this.orbs_visible = false;
    for (let l of this.lanes) {
        switch (this.origin) {
          case "N":
          case "S":
            for (let pm of getPartyList()) {
              if (pm.isActive && pm.x >= 100 + (l-4)*lane_size && pm.x <= 100 + (l-3)*lane_size) {
                pm.ko();
              }
            }
            break;
          case "W":
          case "E":
            for (let pm of getPartyList()) {
              if (pm.isActive && pm.y >= 100 + (l-4)*lane_size && pm.y <= 100 + (l-3)*lane_size) {
                pm.ko();
              }
            }
            break;
        }
    }
  }
  
  update(msSinceLastUpdate) {
    if (this.clone_visible) {
      if (Math.abs(this.clone_x - 100) >= max_clone_distance) {
        this.clone_x_speed = 0;
      }
      if (Math.abs(this.clone_y - 100) >= max_clone_distance) {
        this.clone_y_speed = 0;
      }
      this.clone_x += this.clone_x_speed * msSinceLastUpdate / 1000;
      this.clone_y += this.clone_y_speed * msSinceLastUpdate / 1000;
    }
  }
  
  draw() {
    if (this.clone_visible) {
        drawCircle(this.clone_x, this.clone_y, clone_size, "rgb(124,87,197)");
    }
    if (this.orbs_visible) {
      // draw orbs
      for (let i of this.lanes) {
        let x = 0; let y= 0;
        switch (this.origin) {
          case "N":
            x = (i-3.5)*lane_size + 100;
            y = 85 - 0.5 * lane_size;
            break;
          case "S":
            x = (i-3.5)*lane_size + 100;
            y = 115 + 0.5 * lane_size;
            break;
          case "W":
            y = (i-3.5)*lane_size + 100;
            x = 85 - 0.5 * lane_size;
            break;
          case "E":
            y = (i-3.5)*lane_size + 100;
            x = 115 + 0.5 * lane_size;
            break;
        }
        drawCircle(x, y, orb_size, orb_color, orb_outline_color);
      }
    }
    if (this.warnings_visible) {
      // draw indicators
      switch (this.origin) {
        case "N":
        case "S":
          for (let i of this.lanes) {
            drawRectangle((i-4)*lane_size + 100,85,lane_size,6*lane_size,"rgba(255,85,0,0.7)");
          }
          break;
        case "W":
        case "E":
          for (let i of this.lanes) {
            drawRectangle(85,(i-4)*lane_size + 100,6*lane_size,lane_size,"rgba(255,85,0,0.7)");
          }
          break;
      }
    }
    if (this.hit_indicator_visible) {
      switch (this.origin) {
        case "N":
        case "S":
          for (let i of this.lanes) {
            drawRectangle((i-4)*lane_size + 100,85,lane_size,6*lane_size,orb_hit_effect_color);
          }
          break;
        case "W":
        case "E":
          for (let i of this.lanes) {
            drawRectangle(85,(i-4)*lane_size + 100,6*lane_size,lane_size,orb_hit_effect_color);
          }
          break;
      }
    }
  }
}

const ice_spikes_color ="rgb(198,255,255)";
const ice_spikes_effect_color = "rgba(198,255,255,0.7)";

class IceSpikes extends SelfDrawingEntity {
  #faceNS;
  #hitNWSE;
  #hasHit;

  constructor(faceNS, hitNWSE) {
    super(0);
    this.#faceNS = faceNS;
    this.#hitNWSE = hitNWSE;
    this.#hasHit = false;
  }

  hit() {
    for (let pm of getPartyList()) {
        if (this.#hitNWSE) {
            if ((pm.x <= 100 && pm.y <= 100) || (pm.x >= 100 && pm.y >= 100)) {
                pm.ko();
            }
        } else {
            if ((pm.x <= 100 && pm.y >= 100) || (pm.x >= 100 && pm.y <= 100)) {
                pm.ko();
            }
        }
    }
    this.#hasHit = true;
  }
  
  draw() {
    if (!this.#hasHit) {
      if (this.#faceNS) {
        drawLine(85, 100, 115, 100, ice_spikes_color);
        let spikeTips = this.#hitNWSE ? 1.5 : -1.5;
        for (let i = 0; i < 6; i++) {
          drawPolyline(
            [{x: 102.3 + i*2.5, y: 100},
            {x: 102.5 + i*2.5, y: 100 + spikeTips},
            {x: 102.7 + i*2.5, y: 100}],
            ice_spikes_color
          );
          drawPolyline(
            [{x: 97.7 - i*2.5, y: 100},
            {x: 97.5 - i*2.5, y: 100 - spikeTips},
            {x: 97.3 - i*2.5, y: 100}],
            ice_spikes_color
          );
        }
      } else {
        drawLine(100, 85, 100, 115, ice_spikes_color);
        let spikeTips = this.#hitNWSE ? 1.5 : -1.5;
        for (let i = 0; i < 6; i++) {
          drawPolyline(
            [{x: 100, y: 102.3 + i*2.5},
            {x: 100 + spikeTips, y: 102.5 + i*2.5},
            {x: 100, y: 102.7 + i*2.5}],
            ice_spikes_color
          );
          drawPolyline(
            [{x: 100, y: 97.7 - i*2.5},
            {x: 100 - spikeTips, y: 97.5 - i*2.5},
            {x: 100, y: 97.3 - i*2.5}],
            ice_spikes_color
          );
        }
      }
    } else {
      if (this.#hitNWSE) {
        drawRectangle(85, 85, 15, 15, ice_spikes_effect_color);
        drawRectangle(100, 100, 15, 15, ice_spikes_effect_color);
      } else {
        drawRectangle(85, 100, 15, 15, ice_spikes_effect_color);
        drawRectangle(100, 85, 15, 15, ice_spikes_effect_color);
      }
    }
  }
}

export { OrbSet, IceSpikes };