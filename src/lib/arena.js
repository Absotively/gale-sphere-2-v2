import { SelfDrawingEntity } from "./entities.js";
import { drawCenteredRectangle, getCanvasSize } from "./drawing.js";
import { getPartyList } from "./party.js";

class SquareArena extends SelfDrawingEntity {
    #size;
    #color;
    #left;
    #right;
    #top;
    #bottom;

    constructor(size, color, backgroundColor) {
        super(-1);
        this.#size = size;
        this.#color = color;
        
        this.#left = 100 - size / 2;
        this.#right = 100 + size / 2;
        this.#top = 100 - size / 2;
        this.#bottom = 100 + size / 2;
    }

    update(msSinceLastUpdate) {
        for (let pm of getPartyList()) {
            if (pm.x < this.#left || pm.x > this.#right || pm.y < this.#top || pm.y > this.#bottom) {
                pm.ko();
            }
        }
    }

    draw() {
        let backgroundSize = getCanvasSize() + 1;
        drawCenteredRectangle(100, 100, this.#size, this.#size, this.#color);
    }
}

export { SquareArena };