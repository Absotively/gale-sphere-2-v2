const theoreticalTicksPerSecond = 60;

// https://www.youtube.com/watch?v=hZRQxaDK98o
const partyMemberSpeedInYalmsPerSecond = 3.58;

export { theoreticalTicksPerSecond, partyMemberSpeedInYalmsPerSecond };