import { rollupPluginHTML as html } from '@web/rollup-plugin-html';
import terser from '@rollup/plugin-terser';

export default {
  input: 'src/index.html',
  output: {
    dir: 'dist',
    plugins: [terser()]
  },
  plugins: [
    html({
      minify: true,
    }),
  ],
};
