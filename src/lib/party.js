import { SelfDrawingEntity } from "./entities.js";
import { partyMemberSpeedInYalmsPerSecond } from "./constants.js";
import { getMovementUnitVector } from "./input.js";
import { drawCircle, drawCenteredText, drawX } from "./drawing.js";

let party = {};

function initializeParty(KO_callback) {
  party.T1 = new PartyMember("T1","rgb(0,66,197)", 1, false, false, KO_callback);
  party.H1 = new PartyMember("H1","rgb(44,197,44)", 1, false, true, KO_callback);
  party.M1 = new PartyMember("M1","#f00", 1, true, false, KO_callback);
  party.R1 = new PartyMember("R1","#f00", 1, true, true, KO_callback);
  party.T2 = new PartyMember("T2","rgb(0,66,197)", 2, false, false, KO_callback);
  party.H2 = new PartyMember("H2","rgb(44,197,44)", 2, false, true, KO_callback);
  party.M2 = new PartyMember("M2","#f00", 2, true, false, KO_callback);
  party.R2 = new PartyMember("R2","#f00", 2, true, true, KO_callback);
}

function getPartyList() {
    return Object.values(party);
}

function resetParty() {
  for (let pm of getPartyList()) {
    pm.reset();
  }
}

const party_member_size = 2.5;
const party_member_text_size = 0.7 * party_member_size;

class PartyMember extends SelfDrawingEntity {
  constructor(label, color, lp, is_dps, is_ranged, KO_callback) {
    super(1);
    this.label = label;
    this.color = color;
    this.KOd = false;
    this.isActive = true;
    this.lp = lp;
    this.is_dps = is_dps;
    this.is_ranged = is_ranged;
    this.KO_callback = KO_callback;
    this.target_positions = [];
    this.player_controlled = false;
  }

  reset() {
    this.target_positions = [];
    this.player_controlled = false;
    this.layer = 1;
    this.KOd = false;
    this.isActive = true;
  }
  
  set_position(x, y) {
    this.x = x;
    this.y = y;
  }

  set_target_positions(list) {
    this.target_positions = list;
  }

  add_target_position(x,y) {
    this.target_positions.push({x: x, y: y});
  }
  
  ko() {
    this.KOd = true;
  }
  
  update(msSinceLastUpdate) {
    if (this.KOd) {
        if (this.isActive) {
            this.isActive = false;
            this.KO_callback();
        }
        return;
    }
    let speed = partyMemberSpeedInYalmsPerSecond * msSinceLastUpdate / 1000;
    if (this.player_controlled) {
      let v = getMovementUnitVector();
      this.x += v.x * speed;
      this.y += v.y * speed;
    } else {
      let movement_remaining = speed;
      while (this.target_positions.length > 0 && movement_remaining > 0) {
        let next_target_x_distance = this.target_positions[0].x - this.x;
        let next_target_y_distance = this.target_positions[0].y - this.y;
        let next_target_distance = Math.sqrt(next_target_x_distance * next_target_x_distance + next_target_y_distance * next_target_y_distance);
        if (next_target_distance <= movement_remaining) {
            this.x = this.target_positions[0].x;
            this.y = this.target_positions[0].y;
            movement_remaining -= next_target_distance;
            this.target_positions.shift();
        } else {
            this.x += movement_remaining * next_target_x_distance / next_target_distance;
            this.y += movement_remaining * next_target_y_distance / next_target_distance;
            movement_remaining = 0;
        }
      }
    }
  }
  
  draw() {
    drawCircle(this.x, this.y, party_member_size, this.color, this.player_controlled ? "#fff" : "#000");
    drawCenteredText(this.label, this.x, this.y, party_member_text_size, "monospace", "#fff");
    if (this.KOd) {
      drawX(this.x, this.y, party_member_size, "#000");
    }
  }
}

export { initializeParty, getPartyList, resetParty };
