import { initializeInput, updateInput, clearButtonPresses, wasMenuPressed, wasResetPressed } from "./lib/input.js";
import { drawCenteredRectangle, initializeDrawing } from "./lib/drawing.js";
import { initializeParty, getPartyList, resetParty } from "./lib/party.js";
import { Castbar, BossSpeech, Success, RestartInfo } from "./lib/info_displays.js";
import { LPStacks, Enums } from "./lib/mechanics.js";
import { OrbSet, IceSpikes } from "./mechanics.js";
import { Entities } from "./lib/entities.js";
import { Timeline } from "./lib/timeline.js";
import { theoreticalTicksPerSecond } from "./lib/constants.js";
import { SquareArena } from "./lib/arena.js";

let entities = null;
let rngChoices = {};
let timeline = null;
let intervalId = null;
let animFrameRequestId = null;
let canvasSize = 40; // yalms
let running = false;
let menuDialog = document.getElementById("menu");
let lastUpdateTime = 0;

// init that only happens once
function init() {
    // stuff I should roll up into an init function in lib or something
    initializeInput();
    initializeDrawing(document.getElementById("sim"), canvasSize);
    initializeParty(endRun);
    document.getElementById("start").addEventListener("click", (evt) => { start(); });
    menuDialog.addEventListener("cancel", (evt) => {
        // can't see a good way to prevent closing menu with escape, so just
        // treat it like the user clicked start, it'll be fine probably
        start();
    });
    menuDialog.showModal();
}

// includes all per-pull init
function start() {
    resetParty();

    // stuff I should roll up into an init function in lib or something
    entities = new Entities();
    timeline = new Timeline();
    entities.add(timeline);

    // stuff that's actually for this sim
    rollRNG();
    entities.add(new SquareArena(30, "rgb(105,23,23)"));
    setUpMechanics();
    setUpDutySupport();

    // this should also be handled elsewhere probably
    for (let partyMember of getPartyList()) {
        if (document.querySelector(`input[type="radio"][name="player"][value="${partyMember.label}"]`).checked) {
            partyMember.player_controlled = true;
            partyMember.layer = 2;  // magic numbers: they're terrible but also less initial work
        }
        entities.add(partyMember);
    }
    if (intervalId !== null) {
      clearInterval(intervalId);
    }
    intervalId = setInterval(update, 1000 / theoreticalTicksPerSecond);

    lastUpdateTime = Date.now();
    timeline.start();
    running = true;
}

// most of this should definitely be in some kind of library function or whatever
function update() {
  let time = Date.now();
  let msSinceLastUpdate = time - lastUpdateTime;
  lastUpdateTime = time;
  updateInput();
  if (running) {
    entities.update(msSinceLastUpdate);
  } else if (!menuDialog.open) {
    if (wasMenuPressed()) {
      menuDialog.showModal();
    } else if (wasResetPressed()) {
        start();
    }
  } 
  if (animFrameRequestId === null) {
    animFrameRequestId = window.requestAnimationFrame(draw);
  }
}

function draw() {
  animFrameRequestId = null;
  drawCenteredRectangle(100, 100, canvasSize + 1, canvasSize + 1, "rgb(43,0,0)");
  entities.draw();
}

init();

function endRun() {
  running = false;
  entities.add(new RestartInfo("rgb(43,0,0)"));
  clearButtonPresses();
}

function rollRNG() {
    rngChoices.orb_origins = [];
    rngChoices.orb_lanesets = [];
    if (Math.random() < 0.5) {
      rngChoices.orb_origins.push("N");
      rngChoices.orb_origins.push("S");
    } else {
      rngChoices.orb_origins.push("S");
      rngChoices.orb_origins.push("N");
    }

    // the possible arrangements of safe lanes for the first two sets
    // of orbs are: W, middle; E, middle; middle, W; middle, E
    let dieRoll = Math.random();
    if (dieRoll < 0.25) {
      rngChoices.orb_lanesets.push([3,4,5,6]);
      rngChoices.orb_lanesets.push([1,2,5,6]);
    } else if (dieRoll < 0.5) {
      rngChoices.orb_lanesets.push([1,2,3,4]);
      rngChoices.orb_lanesets.push([1,2,5,6]);
    } else if (dieRoll < 0.75) {
      rngChoices.orb_lanesets.push([1,2,5,6]);
      rngChoices.orb_lanesets.push([3,4,5,6]);
    } else {
      rngChoices.orb_lanesets.push([1,2,5,6]);
      rngChoices.orb_lanesets.push([1,2,3,4]);
    }
    
    if (Math.random() < 0.5) {
      rngChoices.orb_origins.push("E");
      rngChoices.orb_origins.push("W");
    } else {
      rngChoices.orb_origins.push("W");
      rngChoices.orb_origins.push("E");
    }
    
    // safe lanes for the E-W orbs are either N, S or S, N
    if (Math.random() < 0.5) {
      rngChoices.orb_lanesets.push([1,2,3,4]);
      rngChoices.orb_lanesets.push([3,4,5,6]);
    } else {
      rngChoices.orb_lanesets.push([3,4,5,6]);
      rngChoices.orb_lanesets.push([1,2,3,4]);
    }
    
    if (Math.random() < 0.5) {
      rngChoices.iceSpikesFaceNS = true;
    } else {
      rngChoices.iceSpikesFaceNS = false;
    }
    
    if (Math.random() < 0.5) {
      rngChoices.iceSpikesHitNWSE = true;
    } else {
      rngChoices.iceSpikesHitNWSE = false;
    }
    
    if (Math.random() < 0.5) {
      rngChoices.stacksFirst = true;
    } else {
      rngChoices.stacksFirst = false;
    }
    
    if (Math.random() < 0.5) {
      rngChoices.enumsOnDPS = true;
    } else {
      rngChoices.enumsOnDPS = false;
    }
    
    console.log(JSON.stringify(rngChoices));
  }


// all times are in milliseconds
const next_clone_delay = 1000;
const next_orbs_hit_delay = 4000;

const orbs_warnings_duration = 1000;
const hit_effect_duration = 750;

const speech_appears_time = 1000;
const castbar_start_time = 2000;
const speech_disappears_time = 4500;
const castbar_end_time = 5000;
const first_clone_time = 7000;
const orbs_appear_time = 12500;
const first_orbs_hit_time = 18280;
const ice_spikes_appear_time = first_orbs_hit_time - 500;
const ice_spikes_hit_time = first_orbs_hit_time + next_orbs_hit_delay;
const first_stacks_appear_time = 12000;
const first_stacks_hit_time = first_orbs_hit_time;
const second_stacks_appear_time = 24000;
const second_stacks_hit_time = first_orbs_hit_time + 3*next_orbs_hit_delay;
const success_time = second_stacks_hit_time + 250;

function setUpMechanics() {
    let rendYouAsunder = new BossSpeech("A raging gale to rend you asunder!");
    timeline.addEvent(speech_appears_time, () => { entities.add(rendYouAsunder); });
    timeline.addEvent(speech_disappears_time, () => { entities.remove(rendYouAsunder); });

    let castbar = new Castbar("Gale Sphere", castbar_end_time - castbar_start_time);
    timeline.addEvent(castbar_start_time, () => { entities.add(castbar); });
    timeline.addEvent(castbar_end_time, () => { entities.remove(castbar); });

    let enums = new Enums(rngChoices.enumsOnDPS);
    let stacks = new LPStacks();
    let enums_appear_time = first_stacks_appear_time;
    let enums_hit_time = first_stacks_hit_time;
    let lp_stacks_appear_time = second_stacks_appear_time;
    let lp_stacks_hit_time = second_stacks_hit_time;
    if (rngChoices.stacksFirst) {
        enums_appear_time = second_stacks_appear_time;
        enums_hit_time = second_stacks_hit_time;
        lp_stacks_appear_time = first_stacks_appear_time;
        lp_stacks_hit_time = first_stacks_hit_time;
    }
    timeline.addEvent(enums_appear_time, () => { entities.add(enums); });
    timeline.addEvent(enums_hit_time, () => { enums.hit(); });
    timeline.addEvent(enums_hit_time + hit_effect_duration, () => { entities.remove(enums); });
    timeline.addEvent(lp_stacks_appear_time, () => { entities.add(stacks); });
    timeline.addEvent(lp_stacks_hit_time, () => { stacks.hit(); });
    timeline.addEvent(lp_stacks_hit_time + hit_effect_duration, () => { entities.remove(stacks); });

    let clone_time = first_clone_time;
    let hit_time = first_orbs_hit_time;
    let orb_set_list = [];
    for (let i = 0; i < 4; i++) {
        let orbSet = new OrbSet(rngChoices.orb_origins[i], rngChoices.orb_lanesets[i]);
        timeline.addEvent(clone_time, () => { entities.add(orbSet); });
        timeline.addEvent(hit_time - orbs_warnings_duration, () => { orbSet.showWarnings(); });
        timeline.addEvent(hit_time, () => { orbSet.hit(); });
        timeline.addEvent(hit_time + hit_effect_duration, () => { entities.remove(orbSet); });
        orb_set_list.push(orbSet);
        clone_time += next_clone_delay;
        hit_time += next_orbs_hit_delay;
    }
    timeline.addEvent(orbs_appear_time, () => {
        for (let o of orb_set_list) {
            o.showOrbs();
        }
    });

    let iceSpikes = new IceSpikes(rngChoices.iceSpikesFaceNS, rngChoices.iceSpikesHitNWSE);
    timeline.addEvent(ice_spikes_appear_time, () => { entities.add(iceSpikes); });
    timeline.addEvent(ice_spikes_hit_time, () => { iceSpikes.hit(); });
    timeline.addEvent(ice_spikes_hit_time + hit_effect_duration, () => { entities.remove(iceSpikes); });
    
    timeline.addEvent(success_time, () => { entities.add(new Success); endRun(); });
}

const thinking_delay = 1500;
const reaction_delay = 400;

function setUpDutySupport() {
    let party = getPartyList();

    // randomize starting positions
    for (let pm of party) {
        if (pm.label == "T1") {
            pm.set_position(blur(100, 2), blur(96, 2));
        } else {
            pm.set_position(blur(100, 7), blur(108, 4));
        }
    }

    // prepositioning during cast
    timeline.addEvent(castbar_start_time + thinking_delay, () => {
        for (let pm of party) {
            let target_x = pm.x;
            let target_y = pm.y;
            if (pm.lp == 1 && pm.y > 98) {
                target_y = blur(94, 2.5);
            } else if (pm.lp == 2) {
                target_y = blur(106, 2.5);
            }
            if (pm.x < 95) {
                target_x = blur(97, 2);
            } else if (pm.x > 105) {
                target_x = blur(103, 2);
            }

            pm.set_target_positions([{x: target_x, y: target_y}]);
        }
    });

    // position for enums if enums first
    timeline.addEvent(first_stacks_appear_time + thinking_delay, () => {
      if (!rngChoices.stacksFirst) {
        for (let pm of party) {
            let target_x = pm.x;
            let target_y = pm.y;
            if (pm.lp == 1) {
                if (pm.is_ranged) {
                    target_y = 92.5;
                } else {
                    target_y = 97.5;
                }
            } else {
                if (pm.is_ranged) {
                    target_y = 107.5;
                } else {
                    target_y = 102.5;
                }
            }
            target_y = blur(target_y, 0.5);
            
            pm.set_target_positions([{x: target_x, y: target_y}]);
        }
      }
    });

    // positioning for first orb hit
    timeline.addEvent(orbs_appear_time + thinking_delay, () => {
        let ideal_target_x = 100;
        if (!rngChoices.orb_lanesets[0].includes(1)) {
            ideal_target_x = 94;
        } else if (!rngChoices.orb_lanesets[0].includes(3)) {
          if (!rngChoices.orb_lanesets[1].includes(1)) {
            ideal_target_x = 96;
          } else {
            ideal_target_x = 104;
          }
        } else {
            ideal_target_x = 106;
        }
        for (let pm of party) {
            let target_y = pm.y;
            if (pm.target_positions.length > 0) {
              target_y = pm.target_positions[pm.target_positions.length - 1].y;
            }
            pm.set_target_positions([{
                x: blur(ideal_target_x, 0.3),
                y: target_y
            }])
        }
    });

    // positioning for second orb hit
    // includes some prepositioning on y-axis for orbs 3
    timeline.addEvent(first_orbs_hit_time + reaction_delay, () => {
      let target_y = 0;
      let target_x = 0;
      
      if (!rngChoices.orb_lanesets[1].includes(1)) {
        if (rngChoices.iceSpikesHitNWSE) {
          if (!rngChoices.orb_lanesets[2].includes(1)) {
            target_y = 101.5;
          } else {
            target_y = 106.5;
          }
        } else {
          if (!rngChoices.orb_lanesets[2].includes(1)) {
            target_y = 93.5;
          } else {
            target_y = 98.5;
          }
        }
        target_x = 93.5;
      } else if (!rngChoices.orb_lanesets[1].includes(3)) {
        if (!rngChoices.orb_lanesets[2].includes(1)) {
          target_y = 93.5;
        } else {
          target_y = 106.5;
        }
        if (rngChoices.iceSpikesHitNWSE) {
          target_x = target_y > 100 ? 97  : 103;
        } else {
          target_x = target_y > 100 ? 103 : 97;
        }
      } else {
        if (rngChoices.iceSpikesHitNWSE) {
          if (!rngChoices.orb_lanesets[2].includes(1)) {
            target_y = 93.5;
          } else {
            target_y = 98.5;
          }
        } else {
          if (!rngChoices.orb_lanesets[2].includes(1)) {
            target_y = 101.5;
          } else {
            target_y = 106.5;
          }
        }
        target_x = 106.5;
      }
      for (let pm of party) {
          pm.set_target_positions([{
              x: blur(target_x, 1),
              y: blur(target_y, 1)
          }])
      }
    });

    let final_target_x_values = {};

    // stack/enum prepositioning - do more movement before orbs 3 to use the
    // time gained by prepositioning for orbs 3
    timeline.addEvent(first_orbs_hit_time + next_orbs_hit_delay + reaction_delay, () => {
      let ideal_target_y = 0;
      if (!rngChoices.orb_lanesets[2].includes(1)) {
        ideal_target_y = 93.5;
      } else {
        ideal_target_y = 106.5;
      }
      for (let pm of getPartyList()) {
        let final_target_x = 0;
        if (!rngChoices.stacksFirst) {
          final_target_x = pm.lp == 1 ? 96 : 104;
        } else if (pm.is_ranged) {
          final_target_x = pm.lp == 1 ? 94 : 106;
        } else {
          final_target_x = pm.lp == 1 ? 98 : 102;;
        }
        final_target_x = blur(final_target_x, 0.5);
        final_target_x_values[pm.label] = final_target_x;
        let intermediate_target_x = (pm.x + 3 * final_target_x) / 4;
        let target_y = blur(ideal_target_y, 1);
        pm.set_target_positions([
          {x: intermediate_target_x, y: target_y},
          {x: final_target_x, y: target_y}
        ]);
      }
    });

    // final positioning!
    timeline.addEvent(first_orbs_hit_time + 2*next_orbs_hit_delay + reaction_delay, () => {
      let ideal_target_y = 0;
      if (!rngChoices.orb_lanesets[3].includes(1)) {
        ideal_target_y = 94;
      } else {
        ideal_target_y = 106;
      }
      for (let pm of getPartyList()) {
        pm.set_target_positions([
          {x: final_target_x_values[pm.label], y: blur(ideal_target_y, 1)}
        ]);
      }
    });
  }
  
function blur(number, max_amount) {
    return number - max_amount + 2 * max_amount * Math.random();
}