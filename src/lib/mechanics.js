import { SelfDrawingEntity } from "./entities.js";
import { getPartyList } from "./party.js";
import { drawCircle } from "./drawing.js";

class Stacks extends SelfDrawingEntity {
    constructor(targets, soak_count, radius, color) {
      super(0);
      this.targets = targets;
      this.soak_count = soak_count;
      this.radius = radius;
      this.color = color;
      this.hasHit = false;
    }

    hit() {
        let hit = [];
        let newFakeTargets = [];
        for (let target of this.targets) {
            let newFakeTarget = { x: target.x, y: target.y };
            let in_stack = [];
            for (let pm of getPartyList()) {
                if (pm.isActive && Math.sqrt(Math.pow(pm.x - newFakeTarget.x, 2) + Math.pow(pm.y - newFakeTarget.y, 2)) < this.radius) {
                    in_stack.push(pm);
                    if (hit.includes(pm)) {
                        pm.ko();
                    } else {
                        hit.push(pm);
                    }
                }
            }
            if (in_stack.length < this.soak_count) {
                for (let pm of in_stack) {
                    pm.ko();
                }
            }
            newFakeTargets.push(newFakeTarget);
        }
        this.targets = newFakeTargets;
        this.hasHit = true;
    }
    
    draw() {
        let color = this.color;
        let fill = null;
        if (this.hasHit) {
            fill = "rgba(5,5,43,0.7)";
            color = null;
        }
        for (const target of this.targets) {
            drawCircle(target.x, target.y, 2 * this.radius, fill, color);
        }
    }
  }
  
  class LPStacks extends Stacks {
    constructor() {
      let targets = [];
      for (let pm of getPartyList()) {
        if (pm.is_ranged && !pm.is_dps) {
          targets.push(pm);
        }
      }
      super(targets, 4, 6,"rgb(255,255,28)");
    }
  }
  
  class Enums extends Stacks {
    constructor(target_dps) {
      let targets = [];
      for (let pm of getPartyList()) {
        if (target_dps == pm.is_dps) {
          targets.push(pm);
        }
      }
      super(targets, 2, 3, "#fff");
    }
  }

  export { Stacks, LPStacks, Enums };