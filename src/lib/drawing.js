/* we use a similar coordinate system to fflogs' replay: coordinates are in yalms,
   and (100,100) is at the centre of the canvas */

let canvas = null;
let drawingContext = null;
let canvasSizeInYalms = 0;
let transform = new DOMMatrix([1, 0, 0, 1, 0, 0]);
let defaultLineWidth = 1;

// large CSS pixel sizes cause weirdness with text rendering, so we use a higher
// resolution for drawing text
const highResolutionScalingFactor = 100;
const highResolutionScalingInverse = 1 / highResolutionScalingFactor;

function initializeDrawing(canvasElement, sizeInYalms) {
    canvas = canvasElement;
    drawingContext = canvas.getContext("2d");
    canvasSizeInYalms = sizeInYalms;
    updateTransform();
    addEventListener("resize", () => { updateTransform(); });
}

function updateTransform() {
    let minSize = Math.min(canvas.width, canvas.height);
    let scale = minSize / canvasSizeInYalms;
    transform.a = scale;
    transform.b = 0;
    transform.c = 0;
    transform.d = scale;
    transform.e = -100 * scale + canvas.width / 2;
    transform.f = -100 * scale + canvas.height / 2;
    drawingContext.setTransform(transform);
    drawingContext.setTransform(25,0,0,25,-2000,-2000);
    defaultLineWidth = minSize / scale / 200;
}

function getCanvasSize() {
    return canvasSizeInYalms;
}

function drawCircle(center_x, center_y, diameter, fillColor, strokeColor = null) {
    drawingContext.beginPath();
    drawingContext.arc(center_x, center_y, diameter / 2, 0, 2*Math.PI);
    if (fillColor !== null) {
        drawingContext.fillStyle = fillColor;
        drawingContext.fill();
    }
    if (strokeColor !== null) {
        drawingContext.lineWidth = defaultLineWidth;
        drawingContext.strokeStyle = strokeColor;
        drawingContext.stroke();
    }
}

function drawCenteredRectangle(center_x, center_y, width, height, fillColor, strokeColor = null) {
    width = Math.abs(width);
    height = Math.abs(height);
    let left = center_x - width / 2;
    let top = center_y - height / 2;
    drawRectangle(left, top, width, height, fillColor, strokeColor);
}

function drawRectangle(left, top, width, height, fillColor, strokeColor = null) {
    if (fillColor !== null) {
        drawingContext.fillStyle = fillColor;
        drawingContext.fillRect(left, top, width, height);
    }
    if (strokeColor !== null) {
        drawingContext.strokeStyle = strokeColor;
        drawingContext.lineWidth = defaultLineWidth;
        drawingContext.strokeRect(left, top, width, height);
    }
}

// if the font family needs quotes, they need to be included in the string passed in
function drawCenteredText(text, center_x, center_y, sizeInYalms, fontFamily, color) {
    drawingContext.save();
    drawingContext.transform(highResolutionScalingInverse, 0, 0, highResolutionScalingInverse, 0, 0);
    drawingContext.font = `${sizeInYalms * highResolutionScalingFactor}px ${fontFamily}`;
    let metrics = drawingContext.measureText(text);
    let left = center_x * highResolutionScalingFactor - metrics.width / 2;
    // y + ascent - (ascent + descent) / 2
    // y + ascent - 0.5 * ascent - 0.5 * descent
    // y + 0.5 * ascent - 0.5 * descent
    // y + 0.5 * (ascent - descent)
    let baseline = center_y * highResolutionScalingFactor + (metrics.actualBoundingBoxAscent - metrics.actualBoundingBoxDescent) / 2;
    drawingContext.fillStyle = color;
    drawingContext.fillText(text, left, baseline);
    drawingContext.restore();
}

function drawHorizontallyCenteredText(text, center_x, baseline_y, sizeInYalms, fontFamily, color) {
    drawingContext.save();
    drawingContext.transform(highResolutionScalingInverse, 0, 0, highResolutionScalingInverse, 0, 0);
    drawingContext.font = `${sizeInYalms * highResolutionScalingFactor}px ${fontFamily}`;
    let metrics = drawingContext.measureText(text);
    let left = center_x * highResolutionScalingFactor - metrics.width / 2;
    drawingContext.fillStyle = color;
    drawingContext.fillText(text, left, baseline_y * highResolutionScalingFactor);
    drawingContext.restore();
}

function drawLeftAlignedText(text, left_x, baseline_y, sizeInYalms, fontFamily, color) {
    drawingContext.save();
    drawingContext.transform(highResolutionScalingInverse, 0, 0, highResolutionScalingInverse, 0, 0);
    drawingContext.font = `${sizeInYalms * highResolutionScalingFactor}px ${fontFamily}`;
    drawingContext.fillStyle = color;
    drawingContext.fillText(text, left_x * highResolutionScalingFactor, baseline_y * highResolutionScalingFactor);
    drawingContext.restore();
}

// used for KO'd party members
function drawX(center_x, center_y, size, color) {
    let corner_offset = size / 10;
    let left = center_x - size / 2;
    let right = center_x + size / 2;
    let top = center_y - size / 2;
    let bottom = center_y + size / 2;
    let points = [
        {x: left + corner_offset, y: top},
        {x: center_x, y: center_y - corner_offset},
        {x: right - corner_offset, y: top},
        {x: right, y: top + corner_offset},
        {x: center_x + corner_offset, y: center_y},
        {x: right, y: bottom - corner_offset},
        {x: right - corner_offset, y: bottom},
        {x: center_x, y: center_y + corner_offset},
        {x: left + corner_offset, y: bottom},
        {x: left, y: bottom - corner_offset},
        {x: center_x - corner_offset, y: center_y},
        {x: left, y: top + corner_offset}
    ];
    drawPolygon(points, true, color);
}

function drawPolygon(points, close, fillColor, strokeColor = null) {
    let p = new Path2D();
    p.moveTo(points[0].x, points[0].y);
    for (const point of points.slice(1)) {
        p.lineTo(point.x, point.y);
    }
    if (close) {
        p.closePath();
    }
    if (fillColor !== null) {
        drawingContext.fillStyle = fillColor;
        drawingContext.fill(p);
    }
    if (strokeColor !== null) {
        drawingContext.strokeStyle = strokeColor;
        drawingContext.stroke(p);
    }
}

function drawPolyline(points, color) {
    drawPolygon(points, false, null, color);
}

function drawLine(x1, y1, x2, y2, color) {
    drawingContext.beginPath();
    drawingContext.moveTo(x1, y1);
    drawingContext.lineTo(x2, y2);
    drawingContext.strokeStyle = color;
    drawingContext.stroke();
}

export { initializeDrawing, getCanvasSize, drawCircle, drawRectangle, drawCenteredRectangle, drawCenteredText, drawHorizontallyCenteredText, drawLeftAlignedText, drawPolygon, drawLine, drawPolyline, drawX };