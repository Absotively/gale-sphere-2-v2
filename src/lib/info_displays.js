import { getCanvasSize, drawHorizontallyCenteredText, drawLeftAlignedText, drawRectangle } from "./drawing.js";
import { SelfDrawingEntity } from "./entities.js";

const font = "sans-serif";

class Castbar extends SelfDrawingEntity {
    #text;
    #duration;
    #time;
    #amountFilled;
    #width;
    #left;

    constructor(text, duration) {
      super(0);
      this.#text = text;
      this.#duration = duration;
      this.#time = 0;
      this.#amountFilled = 0;
      this.#width = 0.75 * getCanvasSize();
      this.#left = 100 - this.#width / 2;
    }

    update(msSinceLastUpdate) {
        this.#time += msSinceLastUpdate;
        if (this.#time >= this.#duration) {
            this.#amountFilled = 1;
        } else {
            this.#amountFilled = this.#time / this.#duration;
        }
    }
    
    draw() {
        if (this.#time <= this.#duration) {
            let canvasSize = getCanvasSize();
            drawLeftAlignedText(this.#text, this.#left, 97 + canvasSize / 2, 1.5, font, "#fff");
            drawRectangle(this.#left, 98 + canvasSize / 2, 30 * this.#amountFilled, 1, "rgb(255,189,57)");
            drawRectangle(this.#left, 98 + canvasSize / 2, 30, 1, null, "#fff");
        }
    }
  }

class BossSpeech extends SelfDrawingEntity {
    #text

    constructor(text) {
        super(0);
        this.#text = text;
    }

    draw() {
        drawHorizontallyCenteredText(this.#text, 100, 104 - getCanvasSize() / 2, 2, font, "#fff");
    }
}
  
  class Success extends SelfDrawingEntity {
    constructor() {
      super(1);
    }
  
    draw() {
        drawHorizontallyCenteredText("Success!", 100, 102.5, 5, font, "#fff");
    }
  }

  class RestartInfo extends SelfDrawingEntity {
    #backgroundColor = null;

    constructor(backgroundColor) {
        super(1);
        this.#backgroundColor = backgroundColor;
    }
    
    draw() {
        let canvasSize = getCanvasSize();
        if (this.#backgroundColor !== null) {
            drawRectangle(100 - canvasSize / 2, 95 + canvasSize / 2, canvasSize, 6, this.#backgroundColor);
        }
        drawHorizontallyCenteredText("Press SPACE or ENTER or gamepad A to restart", 100, 97 + canvasSize / 2, 1.5, font, "#fff");
        drawHorizontallyCenteredText("Press ESC or the gamepad menu button to change settings", 100, 99 + canvasSize / 2, 1.5, font, "#fff");
    }
  }

  export { Castbar, BossSpeech, Success, RestartInfo };