class SelfDrawingEntity {
    constructor(layer) {
        this.layer = layer;
    }
    update(msSinceLastUpdate) {}
    draw() {}
}

class UpdateOnlyEntity {
    update(msSinceLastUpdate) {}
}

class Entities {
    #entities;
    #layers;
    
    constructor() {
        this.#entities = [];
        this.#layers = [];
    }
    add(entity) {
        this.#entities.push(entity);
        if (entity.hasOwnProperty("layer") && !this.#layers.includes(entity.layer)) {
            this.#layers.push(entity.layer);
            this.#layers.sort((a,b) => a - b);
        }
        return this.#entities.length - 1;
    }
    remove(entity) {
        for (let [index, value] of this.#entities.entries()) {
            if (Object.is(entity, value)) {
                this.#entities[index] = null;
            }
        }
    }
    update(msSinceLastUpdate) { 
        for (let e of this.#entities) {
            if (e && e.update) {
                e.update(msSinceLastUpdate);
            }
        }
    }
    draw() {
        for (const l of this.#layers) {
            for (let e of this.#entities) {
                if (e && e.draw && e.layer == l) {
                    e.draw();
                }
            }
        }
    }
}

export { SelfDrawingEntity, UpdateOnlyEntity, Entities };