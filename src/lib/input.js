const relevantHeldKeys = new Set(["KeyW", "KeyA", "KeyS", "KeyD", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"]);
let pressedKeys = new Set();
const relevantPressedKeys = new Set(["Escape", "Enter", "Space"]    );
const relevantGamepadButtons = [{name: "GamepadA", index: 0}, {name: "GamepadMenu", index: 9}];
let pressedGamepadButtons = new Set();
let unhandledKeyAndButtonPresses = new Set();

function initializeInput() {
    window.addEventListener("keydown", (evt) => {
        if (relevantHeldKeys.has(evt.code)) {
            pressedKeys.add(evt.code);
        }
    });
    window.addEventListener("keyup", (evt) => {
        pressedKeys.delete(evt.code);
        if (relevantPressedKeys.has(evt.code)) {
            unhandledKeyAndButtonPresses.add(evt.code);
        }
    });
}

function updateInput() {
    // multi-gamepad handling is not actually good here, but it shouldn't really need to be
    if (Navigator.getGamepads) {
        for (let gamepad of Navigator.getGamepads()) {
            for (let button of relevantGamepadButtons) {
                if (gamepad.buttons[button.index].pressed) {
                    pressedGamepadButtons.add(button.name);
                } else if (pressedGamepadButtons.has(button.name)) {
                    pressedGamepadButtons.delete(button.name);
                    unhandledKeyAndButtonPresses.add(button.name);
                    console.log(unhandledKeyAndButtonPresses);
                }
            }
        }
    }
}

function clearButtonPresses() {
    unhandledKeyAndButtonPresses.clear();
}

function wasResetPressed() {
    if (unhandledKeyAndButtonPresses.has("Space") ||
        unhandledKeyAndButtonPresses.has("Enter") ||
        unhandledKeyAndButtonPresses.has("GamepadA")) {
        unhandledKeyAndButtonPresses.delete("Space");
        unhandledKeyAndButtonPresses.delete("Enter");
        unhandledKeyAndButtonPresses.delete("GamepadA");
        return true;
    } else {
        return false;
    }
}

function wasMenuPressed() {
    if (unhandledKeyAndButtonPresses.has("Escape") ||
    unhandledKeyAndButtonPresses.has("GamepadMenu")) {
        unhandledKeyAndButtonPresses.delete("Escape");
        unhandledKeyAndButtonPresses.delete("GamepadMenu");
        return true;
    } else {
        return false;
    }
}

function getMovementUnitVector() {
    let vector = {x: 0, y: 0};
    if (pressedKeys.has("ArrowUp") || pressedKeys.has("KeyW")) {
        vector.y = -1;
    } else if (pressedKeys.has("ArrowDown") || pressedKeys.has("KeyS")) {
        vector.y = 1;
    }
    if (pressedKeys.has("ArrowRight") || pressedKeys.has("KeyD")) {
        vector.x = 1;
    } else if (pressedKeys.has("ArrowLeft") || pressedKeys.has("KeyA")) {
        vector.x = -1;
    }
    if (vector.x != 0 || vector.y != 0) {
        if (vector.x == 0 || vector.y == 0) {
            return vector;
        } else {
            let length = Math.sqrt(2);
            vector.x /= length;
            vector.y /= length;
            return vector;
        }
    } else if (Navigator.getGamepads) {
        for (let gamepad of Navigator.getGamepads()) {
            if (gamepad === null) {
                continue;
            }
            if (gamepad.mapping != "standard") {
                continue;
            }
            vector.x = gamepad.axes[0];
            vector.y = gamepad.axes[1];
            let length = Math.sqrt(vector.x * vector.x + vector.y * vector.y);
            if (length >= 0.5) {
                vector.x /= length;
                vector.y /= length;
                return vector;
            }
        }
    }
    return vector;
}

export { initializeInput, updateInput, clearButtonPresses, wasMenuPressed, wasResetPressed, getMovementUnitVector };